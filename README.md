#Sobre o desafio
Este desafio foi feito utilizando o Cucumber e appium, e a linguagem utilizada foi o Ruby versão 2.2.4. 

Os testes foram desenvolvidos para serem executados em um dispositivo Android 6.0

Pré-requisitos
Ruby 2.2.4:

https://www.ruby-lang.org/pt/

cucumber e appium 

Executando

1 - Iniciar o servidor do appium.

2 - Execute no diretório onde estão os arquivos feature:

cucumber


# Objetivo do Desafio

Verificar suas habilidades em conceber cenários de testes e em programação, necessárias para automatização dos testes.

Para isso você deverá conceber cenários para duas funcionalidades do Whatsapp e resolver o exercício 09 do site [CodeKata](http://www.codekata.com), que também pode ser encontrado [aqui](Kata09.md).

### Deve conter ###

* Especificação de duas funcionalidades do Whatsapp (não importa se o app é Android, iOS, Windows Phone ou versão Web) em Gherkin ou Gauge
* Cada especificação deve conter ao menos um cenário de teste
* Resposta do exercício 09 do site [CodeKata](http://www.codekata.com), ou [aqui](Kata09.md), na linguagem de programação que lhe for mais conveniente


### **Processo de submissão** ###
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:
1. Candidato fará um fork desse repositório (não irá clonar direto!)
2. Fará seu projeto nesse fork.
3. Commitará e subirá as alterações para o SEU fork.
4. Pela interface do Bitbucket, irá enviar um Pull Request.

### **ATENÇÃO** ###
Não se deve tentar fazer o PUSH diretamente para ESTE repositório!!!