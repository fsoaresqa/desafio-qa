require "appium_lib"

def caps    
    {caps: { 
    deviceName: "LGG4",
    platformName: "Android", 
    noReset: true,
    appPackage: "com.whatsapp",
    appActivity: "com.whatsapp.HomeActivity", 
    newCommandTimeout: "3600"
    
    }}
    
end


Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object

