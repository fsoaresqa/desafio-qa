
Given("que a aplicacao esta aberta") do
    find_element(id: "tab").displayed? == true
end
Given("Acessar menu mais opcoes") do
   find_element(:accessibility_id, "Mais opções").click
end
Given("Acessar o menu Configuracoes") do
  find_element(:uiautomator, 'new UiSelector().text("Configurações")').click
end
Given("Acionar edicao de perfil") do
  find_element(id: "profile_info_name").click
end
Given("Acionar lapis") do
  find_element(id: "change_registration_name_btn").click
end
Given("Informar o nome alterado {string}") do |string|
  find_element(id: "edit_text").clear
  find_element(id: "edit_text").send_keys(string)
end
Then("Acionar Botao para salvar") do
  find_element(id: "ok_btn").click
end
