
Given("que a aplicacao esteja aberta na tela de conversas") do 
  
  find_element(id: "tab").displayed? == true

end

Given("Acessar menu Lupa") do
  find_element(id: "menuitem_search").click
   
  end
        
  When("Realizar pesquisa contato {string}") do |string|
    find_element(id: "search_src_text").send_keys(string)
    
  end
  
  Then("Pesquisa Realizada com sucesso") do

    find_element(id: "conversations_row_contact_name").displayed? == true
    
  end