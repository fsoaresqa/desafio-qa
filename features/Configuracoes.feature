#encoding: iso-8859-1

Feature: Configuracoess
    Como um usuario
    Eu quero poder alterar configuracoes
    De modo que eu possa personalizar o aplicativo

Background:
    Given que a aplicacao esta aberta

  Scenario Outline: Alterar Nome
    And Acessar menu mais opcoes
    And Acessar o menu Configuracoes
    And Acionar edicao de perfil
    And Acionar lapis
   And Informar o nome alterado <nome>
   Then Acionar Botao para salvar

   Examples:
     | nome |
     |"Nando"|

