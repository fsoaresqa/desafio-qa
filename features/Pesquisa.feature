#encoding: iso-8859-1

Feature: Pesquisar
    Como um usuario
    Eu quero pode realizar pesquisas
    De modo que eu encontre o que e esperado

Background:
    Given que a aplicacao esteja aberta na tela de conversas

  Scenario: Pesquisa
    And Acessar menu Lupa
    When Realizar pesquisa contato "Casa"
    Then Pesquisa Realizada com sucesso

